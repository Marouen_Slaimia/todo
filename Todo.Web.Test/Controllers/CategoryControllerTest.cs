﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Todo.Common.Models;
using Todo.Service.Interfaces;
using Todo.Web.Controllers;

namespace Todo.Web.Test.Controllers
{
    [TestClass]
    public class CategoryControllerTest
    {
        private List<CategoryModel> categories = new List<CategoryModel>
        {
            new CategoryModel { Id = 1, Title = "First list" },
            new CategoryModel { Id = 2, Title = "Second list" },
        };

        [TestMethod]
        public void List()
        {
            // Arrange
            var mock = new Mock<ICategoryService>();
            mock.Setup(m => m.List()).Returns(() => categories);

            var controller = new CategoryController(mock.Object);

            // Act
            var result = controller.CategoryList() as ViewResult;

            // Assert
            Assert.AreEqual("CategoryList", result.ViewName);
            Assert.IsInstanceOfType(result.ViewData.Model, typeof(List<CategoryModel>));
            var model = (List<CategoryModel>)result.ViewData.Model;
            Assert.AreEqual(2, model.Count);
            Assert.AreEqual(1, model[0].Id);
        }
    }
}

