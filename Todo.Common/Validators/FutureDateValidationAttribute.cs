﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Todo.Common.Validators
{
    public class FutureDateValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var date = value as DateTime?;
            return date == null || date > DateTime.Today;
        }
    }
}