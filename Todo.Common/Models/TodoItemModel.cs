﻿using Todo.Common.Enumerations;
using Todo.Common.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Todo.Common.Models
{
    public class TodoItemModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int CategoryId { get; set; }

        [Display(Name = "Item title")]
        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "The title must have between 3 and 20 characters.")]
        public string Title { get; set; }

        public bool Done { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        [Range(0, 10, ErrorMessage = "The quantity must be 10 or less.")]
        public double? Quantity { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        public Priority? Priority { get; set; }

        [DisplayFormat(NullDisplayText = "-")]
        [DataType(DataType.Date)]
        [Display(Name = "Due Date")]
        [FutureDateValidation(ErrorMessage = "The due date must be in the future.")]
        public DateTime? DueDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? StyleId { get; set; }

        public StyleModel Style { get; set; }
    }
}