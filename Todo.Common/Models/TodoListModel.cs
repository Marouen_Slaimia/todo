﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Todo.Common.Models
{
    public class TodoListModel
    {
        public TodoListModel(List<TodoItemModel> todoItems)
        {
            TodoItems = todoItems;
        }

        public List<TodoItemModel> TodoItems { get; set; }

        public SelectList CategorySelectList { get; set; }
        public int SelectedCategoryId { get; set; }

        public string Header { get; set; }
    }
}