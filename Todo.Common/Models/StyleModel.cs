﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.Common.Models
{
    public class StyleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FontName { get; set; }
        public double? FontSize { get; set; }
        public string ForeColor { get; set; }
        public string BackColor { get; set; }
    }
}
