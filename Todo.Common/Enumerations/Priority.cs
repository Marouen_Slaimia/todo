﻿namespace Todo.Common.Enumerations
{
    public enum Priority
    {
        Low,
        Normal,
        High
    }
}
