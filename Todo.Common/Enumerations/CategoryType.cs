﻿namespace Todo.Common.Enumerations
{
    public enum CategoryType
    {
        Shopping = 1,
        DIY = 2,
        Children = 3
    }
}