﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Common.Models;
using Todo.Service.Interfaces;
using Todo.Service.Services;

namespace Todo.Web.Controllers
{
    [RoutePrefix("TodoList")]
    public class TodoListController : Controller
    {
        private ITodoItemService _todoItemService;
        private ICategoryService _categoryService;
        private IStyleService _styleService;

        public TodoListController(ITodoItemService todoItemService, ICategoryService categoryService, IStyleService styleService)
        {
            _todoItemService = todoItemService;
            _categoryService = categoryService;
            _styleService = styleService;
        }


        // GET: TodoList
        [Route("~/List/{id:int}")]
        [Route("ItemList/{id}")]
        public ActionResult ItemList(int id)
        {
            var model = _todoItemService.List(id);

            var categories = _categoryService.List();
            model.CategorySelectList = new SelectList(categories, "Id", "Title");
            model.SelectedCategoryId = id;
            model.Header = categories.Single(x => x.Id == id).Title;

            return View(model);
        }

        [Route("~/List/{name}")]
        public ActionResult ItemList(string name)
        {
            var model = _todoItemService.ListByName(name);
            return View(model);
        }

        public ActionResult ItemEdit(int id)
        {
            var model = _todoItemService.Read(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult ItemEdit(TodoItemModel model)
        {
            //if (ModelState.IsValid)
            //{
            //    if (model.Quantity.HasValue && model.Quantity < 5 && model.Priority == Common.Enumerations.Priority.High)
            //        ModelState.AddModelError("Priority", "Priority must not be high if quantity is less than 5.");
            //}

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                _todoItemService.Update(model);
            }
            catch (ApplicationException ex)
            {
                ModelState.AddModelError("Priority", ex.Message);
                return View(model);
            }

            return RedirectToAction("ItemList", new { id = model.CategoryId });
        }

        public ActionResult ChangeList(int SelectedCategoryId)
        {
            return RedirectToAction("ItemList", new { id = SelectedCategoryId });
        }

        //---- Display item
        public ActionResult ItemDisplay(int id)
        {
            var item = _todoItemService.Read(id);
            return View(item);
        }

        public ActionResult EditStyle(int? id)
        {
            var list = _styleService.List();
            ViewBag.StyleList = new SelectList(list, "Id", "Name", id);
            return PartialView();
        }

        //---- Get a style details
        public ActionResult GetStyleDetails(int id)
        {
            var item = _styleService.Read(id);
            return Json(item, JsonRequestBehavior.AllowGet);
        }
    }
}