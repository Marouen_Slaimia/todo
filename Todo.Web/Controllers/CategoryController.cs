﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Common.Models;
using Todo.Service.Services;
using Todo.Service.Interfaces;

namespace Todo.Web.Controllers
{
    public class CategoryController : Controller
    {
        ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        // GET: Category
        public ActionResult CategoryList()
        {
            var model = _categoryService.List();
            return View("CategoryList", model);
        }
    }
}