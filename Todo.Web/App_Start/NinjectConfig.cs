﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Service.Interfaces;
using Todo.Service.Services;

namespace Todo.Web
{
    public class NinjectConfig : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectConfig()
        {
            _kernel = new StandardKernel();
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            _kernel.Bind<ICategoryService>().To<CategoryService>();
            _kernel.Bind<ITodoItemService>().To<TodoItemService>();
            _kernel.Bind<IStyleService>().To<StyleService>();
            Todo.Service.Configuration.NinjectConfig.Config(_kernel);
        }
    }
}