﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Todo.Web.Extensions
{
    public static class MyExtensions
    {
        public static HtmlString ImageActionLink(this HtmlHelper html, string linkText, string className, string actionName, string controllerName, object routeValues)
        {
            var urlHelper = new UrlHelper(html.ViewContext.RequestContext);

            // Link
            var link = new TagBuilder("a");
            link.Attributes.Add("href", urlHelper.Action(actionName, controllerName, routeValues));
            link.AddCssClass("list-group-item");

            // Image
            var span = new TagBuilder("span");
            span.AddCssClass(className);

            // Put image and text inside link
            link.InnerHtml = span.ToString(TagRenderMode.SelfClosing) + " " + linkText;

            return new HtmlString(link.ToString());
        }

        public static HtmlString LabelTextBox(this HtmlHelper html, string name, string text, string value)
        {
            var label = new TagBuilder("label");
            label.Attributes.Add("for", name);
            label.SetInnerText(text);

            var textbox = new TagBuilder("input");
            textbox.Attributes.Add("type", "text");
            textbox.Attributes.Add("id", name);
            textbox.Attributes.Add("name", name);
            textbox.Attributes.Add("value", value);
            textbox.AddCssClass("form-control");

            return new HtmlString(label.ToString() + " " + textbox.ToString(TagRenderMode.SelfClosing));
        }

        public static HtmlString LabelTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> sourceExpression)
        {
            var name = ExpressionHelper.GetExpressionText(sourceExpression);
            var metadata = ModelMetadata.FromLambdaExpression(sourceExpression, html.ViewData);
            return LabelTextBox(html, name, metadata.DisplayName, metadata.Model as string);
        }
    }
}