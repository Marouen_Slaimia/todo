﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Todo.Web.Areas.Admin.Controllers
{
    [RouteArea("Admin")]
    [RoutePrefix("Users")]
    [Route("{action}")]
    [Authorize]
    public class UsersController : Controller
    {
        // GET: Admin/Users
        public ActionResult Index()
        {
            return View();
        }
    }
}