﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Repository.Interfaces;
using Todo.Repository.Repositories;

namespace Todo.Service.Configuration
{
    public class NinjectConfig
    {
        public static void Config(IKernel kernel)
        {
            kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            kernel.Bind<ITodoItemRepository>().To<TodoItemRepository>();
            kernel.Bind<IStyleRepository>().To<StyleRepository>();
        }
    }
}
