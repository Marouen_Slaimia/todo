﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Common.Models;
using Todo.Repository;

namespace Todo.Service.Configuration
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.CreateMap<Category, CategoryModel>();
            Mapper.CreateMap<CategoryModel, Category>();

            Mapper.CreateMap<TodoItem, TodoItemModel>()
                .ForMember(m => m.Priority, opt => opt.MapFrom(s => s.Priority));
            Mapper.CreateMap<TodoItemModel, TodoItem>()
                .ForMember(m => m.Priority, opt => opt.MapFrom(s => s.Priority));

            Mapper.CreateMap<Style, StyleModel>();
            Mapper.CreateMap<StyleModel, Style>();
        }
    }
}
