﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Common.Models;
using Todo.Repository;
using Todo.Repository.Interfaces;
using Todo.Repository.Repositories;
using Todo.Service.Interfaces;

namespace Todo.Service.Services
{
    public class CategoryService : ICategoryService
    {
        ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public void Add(CategoryModel categoryModel)
        {
            _categoryRepository.Add(Mapper.Map<Category>(categoryModel));
        }

        public void Delete(CategoryModel categoryModel)
        {
            _categoryRepository.Delete(Mapper.Map<Category>(categoryModel));
        }

        public List<CategoryModel> List()
        {
            return Mapper.Map<List<CategoryModel>>(_categoryRepository.List());
        }

        public CategoryModel Read(int id)
        {
            return Mapper.Map<CategoryModel>(_categoryRepository.Read(id));
        }

        public void Update(CategoryModel categoryModel)
        {
            _categoryRepository.Update(Mapper.Map<Category>(categoryModel));
        }
    }
}
