﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Common.Models;
using Todo.Repository.Interfaces;
using Todo.Service.Interfaces;

namespace Todo.Service.Services
{
    public class StyleService : IStyleService
    {
        private IStyleRepository _styleRepository;

        public StyleService(IStyleRepository styleRepository)
        {
            _styleRepository = styleRepository;
        }

        public List<StyleModel> List()
        {
            return Mapper.Map<List<StyleModel>>(_styleRepository.List());
        }

        public StyleModel Read(int id)
        {
            return Mapper.Map<StyleModel>(_styleRepository.Read(id));
        }
    }
}
