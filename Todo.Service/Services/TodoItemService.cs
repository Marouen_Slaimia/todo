﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Common.Models;
using Todo.Repository;
using Todo.Repository.Interfaces;
using Todo.Service.Interfaces;

namespace Todo.Service.Services
{
    public class TodoItemService : ITodoItemService
    {
        private ITodoItemRepository _todoItemRepository;

        public TodoItemService(ITodoItemRepository todoItemRepository)
        {
            _todoItemRepository = todoItemRepository;
        }

        public TodoListModel List(int categoryId)
        {
            return new TodoListModel(Mapper.Map<List<TodoItemModel>>(_todoItemRepository.List(categoryId)));
        }

        public TodoListModel ListByName(string name)
        {
            return new TodoListModel(Mapper.Map<List<TodoItemModel>>(_todoItemRepository.ListByName(name)));
        }

        public TodoItemModel Read(int id)
        {
            return Mapper.Map<TodoItemModel>(_todoItemRepository.Read(id));
        }

        //---- Add a new item
        public void Add(TodoItemModel todoItem)
        {
            _todoItemRepository.Add(Mapper.Map<TodoItem>(todoItem));
        }

        //---- Modify an existing item
        public void Update(TodoItemModel todoItem)
        {
            if (todoItem.Quantity.HasValue && todoItem.Quantity < 5 && todoItem.Priority == Common.Enumerations.Priority.High)
                throw new ApplicationException("Priority must not be high if quantity is less than 5.");

            _todoItemRepository.Update(Mapper.Map<TodoItem>(todoItem));
        }

        //---- Delete an item
        public void Delete(TodoItemModel todoItem)
        {
            _todoItemRepository.Delete(Mapper.Map<TodoItem>(todoItem));
        }
    }
}
