﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Common.Models;

namespace Todo.Service.Interfaces
{
    public interface ITodoItemService
    {
        TodoListModel List(int categoryId);
        TodoListModel ListByName(string name);
        TodoItemModel Read(int id);
        void Add(TodoItemModel todoItem);
        void Update(TodoItemModel todoItem);
        void Delete(TodoItemModel todoItem);
    }
}
