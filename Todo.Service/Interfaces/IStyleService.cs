﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Common.Models;

namespace Todo.Service.Interfaces
{
    public interface IStyleService
    {
        List<StyleModel> List();
        StyleModel Read(int id);
    }
}
