﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Common.Models;

namespace Todo.Service.Interfaces
{
    public interface ICategoryService
    {
        List<CategoryModel> List();
        CategoryModel Read(int id);
        void Add(CategoryModel categoryModel);
        void Update(CategoryModel categoryModel);
        void Delete(CategoryModel categoryModel);
    }
}
