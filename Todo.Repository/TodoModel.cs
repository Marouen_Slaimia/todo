namespace Todo.Repository
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TodoModel : DbContext
    {
        public TodoModel()
            : base("name=TodoModel")
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Style> Styles { get; set; }
        public virtual DbSet<TodoItem> TodoItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(e => e.TodoItems)
                .WithRequired(e => e.Category)
                .WillCascadeOnDelete(false);
        }
    }
}
