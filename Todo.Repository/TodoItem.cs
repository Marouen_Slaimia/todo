namespace Todo.Repository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TodoItem")]
    public partial class TodoItem
    {
        public int Id { get; set; }

        public int CategoryId { get; set; }

        [Required]
        public string Title { get; set; }

        public bool Done { get; set; }

        public double? Quantity { get; set; }

        public int? Priority { get; set; }

        public DateTime? DueDate { get; set; }

        public int? StyleId { get; set; }

        public byte[] ImageData { get; set; }

        [StringLength(50)]
        public string ImageMimeType { get; set; }

        public virtual Category Category { get; set; }

        public virtual Style Style { get; set; }
    }
}
