﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.Repository.Interfaces
{
    public interface IStyleRepository
    {
        List<Style> List();
        Style Read(int id);
    }
}
