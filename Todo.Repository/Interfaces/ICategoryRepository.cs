﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.Repository.Interfaces
{
    public interface ICategoryRepository
    {
        List<Category> List();
        Category Read(int id);
        void Add(Category category);
        void Update(Category category);
        void Delete(Category category);

    }
}
