﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.Repository.Interfaces
{
    public interface ITodoItemRepository
    {
        List<TodoItem> List(int categoryId);
        List<TodoItem> ListByName(string name);
        TodoItem Read(int id);
        void Add(TodoItem totoItem);
        void Update(TodoItem totoItem);
        void Delete(TodoItem totoItem);
    }
}
