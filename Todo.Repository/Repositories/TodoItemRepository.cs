﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Repository.Interfaces;

namespace Todo.Repository.Repositories
{
    public class TodoItemRepository : ITodoItemRepository
    {
        private TodoModel db = new TodoModel();

        public List<TodoItem> List(int categoryId)
        {
            return db.TodoItems.Where(item => item.CategoryId == categoryId).ToList();
        }

        public List<TodoItem> ListByName(string name)
        {
            var q = from item in db.TodoItems.Include(x => x.Style)
                    join list in db.Categories on item.CategoryId equals list.Id
                    where list.Title == name
                    select item;
            return q.ToList();
        }

        public TodoItem Read(int id)
        {
            return db.TodoItems.Include(x => x.Style).Single(x => x.Id == id);
        }

        //---- Add a new item
        public void Add(TodoItem totoItem)
        {
            db.TodoItems.Add(totoItem);
            db.SaveChanges();
        }

        //---- Modify an existing item
        public void Update(TodoItem totoItem)
        {
            db.TodoItems.Attach(totoItem);
            db.Entry(totoItem).State = EntityState.Modified;
            db.SaveChanges();
        }

        //---- Delete an item
        public void Delete(TodoItem totoItem)
        {
            var lst = db.TodoItems.Find(totoItem.Id);
            db.TodoItems.Remove(lst);
            db.SaveChanges();
        }

    }
}
