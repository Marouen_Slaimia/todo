﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Repository.Interfaces;

namespace Todo.Repository.Repositories
{
    public class StyleRepository : IStyleRepository
    {
        public List<Style> List()
        {
            using (var db = new TodoModel())
            {
                return db.Styles.ToList();
            }
        }

        public Style Read(int id)
        {
            using (var db = new TodoModel())
            {
                return db.Styles.Find(id);
            }
        }
    }
}
