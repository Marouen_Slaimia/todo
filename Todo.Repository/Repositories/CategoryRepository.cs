﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Repository.Interfaces;

namespace Todo.Repository.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        public List<Category> List()
        {
            using (var db = new TodoModel())
            {
                return db.Categories.ToList();
            }
        }

        public Category Read(int id)
        {
            using (var db = new TodoModel())
            {
                return db.Categories.Find(id);
            }
        }

        public void Add(Category category)
        {
            using (var db = new TodoModel())
            {
                db.Categories.Add(category);
                db.SaveChanges();
            }
        }

        public void Update(Category category)
        {
            using (var db = new TodoModel())
            {
                db.Categories.Attach(category);
                db.Entry(category).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(Category category)
        {
            using (var db = new TodoModel())
            {
                var cat = db.Categories.Find(category.Id);
                db.Categories.Remove(cat);
                db.SaveChanges();
            }
        }
    }
}
